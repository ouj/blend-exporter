import bpy


bl_info = {
    "name": "Seperate All by Material",
    "author": "Jiawei Ou",
    "version": (0,1),
    "blender": (2, 6, 3),
    "location": "View3D > Object",
    "description": "Seperate all objects by material",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Object"}

def main(context):
    for ob in context.scene.objects:
        print(ob)
        context.scene.objects.active = ob
        
        edit_on = (context.edit_object != None)
        if not edit_on:
            try:
                bpy.ops.object.editmode_toggle()
                edit_on = True
            except:
                edit_on = False
        
        if edit_on:
            bpy.ops.mesh.separate(type='MATERIAL')
            
        if edit_on:
            try:
                bpy.ops.object.editmode_toggle()
                edit_on = False
            except:
                edit_on = True


class SeperateByMaterialOperator(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "object.separate_by_material"
    bl_label = "Seperate by Material Operator"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        main(context)
        return {'FINISHED'}

def menu_func(self, context):
    self.layout.operator(SeperateByMaterialOperator.bl_idname, text="SeperatebyMaterial", icon='PLUGIN')
    
def register():
    bpy.utils.register_module(__name__)
    # Add "Chain" menu to the "Add Mesh" menu.
    bpy.types.INFO_MT_mesh_add.append(menu_func)


def unregister():
    bpy.utils.unregister_module(__name__)
    # Remove "Chain" menu from the "Add Mesh" menu.
    bpy.types.INFO_MT_mesh_add.remove(menu_func)

if __name__ == "__main__":
    register()
