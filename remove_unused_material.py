import bpy


bl_info = {
    "name": "Remove Unused Material Slot",
    "author": "Jiawei Ou",
    "version": (0,1),
    "blender": (2, 6, 3),
    "location": "View3D > Object",
    "description": "Remove unused material slot",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Object"}

def proc_shape(ob):
    shape = ob.data
    verts = shape.vertices
    triangles = shape.polygons
    indices = list(set([f.material_index for f in triangles]))
    i = 0
    while i < len(shape.materials):
        print(list(shape.materials))
        if i not in indices:
            bpy.context.scene.objects.active = ob
            ob.active_material_index = i
            bpy.ops.object.material_slot_remove()
            indices = [m-1 if m>i else m for m in indices]
            print(indices)
        else:
            i = i + 1
    
def main(context):
    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
    for ob in context.scene.objects:
        print(ob)
        if ob.type == 'MESH' and not ob.hide:
            proc_shape(ob)

class RemoveUnusedMaterialSlot(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "object.remove_unused_material_slot"
    bl_label = "Remove Unused Material Slot"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        main(context)
        return {'FINISHED'}

def menu_func(self, context):
    self.layout.operator(RemoveUnusedMaterialSlot.bl_idname, text="RemoveUnusedMaterialSlot", icon='PLUGIN')
    
def register():
    bpy.utils.register_module(__name__)
    # Add "Chain" menu to the "Add Mesh" menu.
    bpy.types.INFO_MT_mesh_add.append(menu_func)


def unregister():
    bpy.utils.unregister_module(__name__)
    # Remove "Chain" menu from the "Add Mesh" menu.
    bpy.types.INFO_MT_mesh_add.remove(menu_func)

if __name__ == "__main__":
    register()
