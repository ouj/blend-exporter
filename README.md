Overview
========
Blender exporter for near rendering system.

Deployment
=========
Copy folder `io_scene_near` into the `addons` folder. The folder can be found in these locations:

* __Windows 7__ - `C:\Users\%username%\AppData\Roaming\Blender Foundation\Blender\$version\scripts\addons`
* __Windows XP__ - `C:\Documents and Settings\%username%\Application Data\Blender Foundation\Blender\$version\scripts\addons`
* __Linux__ - `/home/$user/.config/blender/$version/scripts/addons`
* __MacOSX__ - `/Applications/blender.app/Contents/MacOS/$version/scripts/addons`

Enabling the exporter in User preferences.

For more detail please follow this link: [http://wiki.blender.org/index.php/Doc:2.6/Manual/Extensions/Python/Add-Ons](http://wiki.blender.org/index.php/Doc:2.6/Manual/Extensions/Python/Add-Ons)

