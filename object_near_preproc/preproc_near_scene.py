import math
import bpy
import bpy.ops

def dereference_objects():
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.duplicates_make_real()
    bpy.ops.object.select_all(action='DESELECT') 

def make_local_objects():
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.make_local(type='SELECTED_OBJECTS_DATA')
    bpy.ops.object.select_all(action='DESELECT') 

def separate_mesh_material(mesh):
    if len(mesh.data.materials) > 1:
        faces = mesh.data.faces
        if len(faces) < 1:
            return
 
    bpy.context.scene.objects.active = mesh
    try:
        bpy.ops.object.editmode_toggle()
    except:
        print('failed to enter editmode mesh: ' + mesh.name)
    bpy.ops.mesh.separate(type='MATERIAL')
    try:
        bpy.ops.object.editmode_toggle()
    except:
        print('failed to leave editmode mesh: ' + mesh.name)

def make_single_user():
    bpy.ops.object.make_single_user(type='ALL', object=True, obdata=True, material=True, texture=True, animation=False)

def separate_meshes():
    bpy.ops.object.select_all(action='DESELECT')
    objects = bpy.context.scene.objects
    for obj in objects:
        if obj.type == 'MESH':
            separate_mesh_material(obj)

def clear_parent():
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.parent_clear(type='CLEAR_KEEP_TRANSFORM')
    bpy.ops.object.select_all(action='SELECT')

def clean_material():
    objects = bpy.context.scene.objects
    for obj in objects:
        if obj.type == 'MESH':
            if len(obj.data.materials) > 1 and len(obj.data.faces) >= 1:
                material_index = obj.data.faces[0].material_index
                for m in range(len(obj.data.materials)):
                    if m != material_index:
                        obj.data.materials.pop(m)

def preprocess(opts):
    if opts['clear_parent'] == True:
        clear_parent()
    if opts['dereference'] == True:
        dereference_objects()
    if opts['make_local'] == True:
        make_local_objects()
    if opts['make_single'] == True:
        make_single_user()
    if opts['separate'] == True:
        separate_meshes()
    return {'FINISHED'}
