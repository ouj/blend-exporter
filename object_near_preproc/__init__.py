# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

bl_info = {
    "name": "Near Scene Preprocess",
    "author": "Jiawei Ou",
    "blender": (2, 5, 6),
    "api": 35357,
    "location": "Object",
    "description": "Preprocess Scene for Near Export",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Object"}

if "bpy" in locals():
    import imp
    if "preproc_near_scene" in locals():
        imp.reload(preproc_near_scene)

import os
import bpy
from bpy.props import *

class PreprocNearSceneOperator(bpy.types.Operator):
    bl_idname = "object.preproc_near_scene"
    bl_label = "Preprocess Near Scene"
    
    use_clear_parent = BoolProperty(name="Clear Parent", description="clear the parent and keep the transform", default=True)
    use_dereference = BoolProperty(name="Dereference Instance", description="dereference the instance", default=True)
    use_make_local = BoolProperty(name="Make Local", description="make local copies", default=True)
    use_make_single = BoolProperty(name="Make Single", description="make all meshes single user", default=True)
    use_separate = BoolProperty(name="Seperate Meshes", description="separate meshes base on their materials", default=True)
    
    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def execute(self, context):
        opts = {}
        opts['clear_parent'] = self.use_clear_parent
        opts["dereference"] = self.use_dereference
        opts['make_local'] = self.use_make_local
        opts["make_single"] = self.use_make_single
        opts["separate"] = self.use_separate
  
        import object_near_preproc.preproc_near_scene
        return object_near_preproc.preproc_near_scene.preprocess(opts)

def register():
    bpy.utils.register_class(PreprocNearSceneOperator)
    # Invoke the dialog when loading
    # bpy.ops.object.dialog_operator('INVOKE_DEFAULT')
    #bpy.utils.register_module(__name__)

def unregister():
    #bpy.utils.unregister_module(__name__)
    return

