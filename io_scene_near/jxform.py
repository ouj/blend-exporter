# Blender matrix is column major
# Blender Camera:
#   right = x
#   up = y
#   forward = -z

# Near Camera:
#   right = x
#   up = -y
#   forward = z
from mathutils import *
from io_scene_near.jutils import *

def matrix_flip_z(mat):
    #flip y and z
    nmat = Matrix()
    for i in range(0, 4):
        nmat[i][0] = mat[i][0]
        nmat[i][1] = -mat[i][1]
        nmat[i][2] = -mat[i][2]
        nmat[i][3] = mat[i][3]
    return nmat

def is_identity(mat):
    for i in range(0, 4):
        for j in range(0, 4):
            if(i == j):
                if not jequal(mat[i][j], 1.0):
                    return False
            else:
                if not jequal(mat[i][j], 0.0):
                    return False
    return True

def is_identity2(location, rotation):
    if location.length_squared != 0:
        return False
    if rotation.x != 0 or rotation.y != 0 or rotation.z != 0:
        return False
    return True

def is_scaled(mat):
    v0 = mat[0].xyz
    v1 = mat[1].xyz
    v2 = mat[2].xyz
    if(not(jequal(v0.dot(v0), 1)
       and jequal(v1.dot(v1), 1)
       and jequal(v2.dot(v2), 1))):
        return True
    if(not(jequal(v0.dot(v1), 0)
       and jequal(v0.dot(v2), 0)
       and jequal(v1.dot(v2), 0))):
        return True
    return False

def convert_frame(mat):
    frame = {}
    frame["x"] = [mat[0][0],mat[1][0],mat[2][0]]
    frame["y"] = [mat[0][1],mat[1][1],mat[2][1]]
    frame["z"] = [mat[0][2],mat[1][2],mat[2][2]]
    frame["o"] = [mat[0][3],mat[1][3],mat[2][3]]
    return frame

def convert_frame2(location, rotation):
    frame = {}
    mat = rotation.to_matrix()
    frame["x"] = [mat[0][0],mat[1][0],mat[2][0]]
    frame["y"] = [mat[0][1],mat[1][1],mat[2][1]]
    frame["z"] = [mat[0][2],mat[1][2],mat[2][2]]
    frame["o"] = [v for v in location]
    return frame
    
def convert_frame_invert_z(location, rotation):
    frame = {}
    mat = rotation.to_matrix()
    frame["x"] = [mat[0][0],mat[1][0],mat[2][0]]
    frame["y"] = [-mat[0][1],-mat[1][1],-mat[2][1]]
    frame["z"] = [-mat[0][2],-mat[1][2],-mat[2][2]]
    frame["o"] = [v for v in location]
    return frame    
    
def get_identitytransform(name):
    xform = {}
    xform["type"] = "identitytransform"
    xform["name"] = name
    return xform
    
def create_identity_transform(jscene):
    xform = get_identitytransform('identity_transform')
    jscene["transforms"].append(xform)

def proc_transform2(location, rotation, name):
    xform = {}
    xform["name"] = name
    if is_identity2(location, rotation) == True:
        xform["type"] = "identitytransform"
    else:
        xform["type"] = "rigidtransform"
        xform["frame"] = convert_frame2(location, rotation)
    return xform
    
def proc_transform_invert_z(location, rotation, name):
    xform = {}
    xform["name"] = name
    xform["type"] = "rigidtransform"
    xform["frame"] = convert_frame_invert_z(location, rotation)
    return xform        

def proc_transform(mat, name):
    xform = {}
    xform["name"] = name
    if is_scaled(mat) == True:
        print('cannot handle scale matrix')
        xform["type"] = "scaletransform"
    elif is_identity(mat) == True:
        xform["type"] = "identitytransform"
    else:
        xform["type"] = "rigidtransform"
        xform["frame"] = convert_frame(mat)
    return xform