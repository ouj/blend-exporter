import os
import bpy
import mathutils
import json
import time
from io_scene_near.jscene import json_scene 
from io_scene_near.convimage import convert_images 

print ('PASSIVE BLENDER EXPORTER')
output_folder = os.getenv('BLD_EXPORT_FOLDER')

opts = {}
opts["diffuse_only"] = False
opts["view_as_camera"] = False
opts["light_power_mult"] = 1.0
opts["inline_geom"] = False
opts["export_material"] = True
opts["apply_modifiers"] = True

(pathname, ext) = os.path.splitext(bpy.data.filepath)
basename = os.path.basename(pathname)
if output_folder == None:
    dirname = os.path.dirname(pathname)
else:
    dirname = output_folder

path = os.path.join(dirname, basename + ".json")

scene = bpy.context.scene
data = bpy.data

start = time.clock()
jscene = json_scene(scene, data, dirname, basename, opts);
scene_string = json.dumps(jscene, indent=4, sort_keys=True)

f = open(path, "w", newline='\n')
f.write(scene_string)
f.close()
convert_images(opts["tex_path"]);
print ("scene: " + basename + ' exported, takes ' + str(time.clock()-start) + ' time unit')





