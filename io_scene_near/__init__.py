# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

bl_info = {
    "name": "Near Scene Format",
    "author": "Jiawei Ou, Jon Denning",
    "blender": (2, 5, 6),
    "api": 35357,
    "location": "File > Export",
    "description": "Export Near Scene",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export"}

if "bpy" in locals():
    import imp
    if "export_near" in locals():
        imp.reload(export_near)


import os
import bpy
from bpy.props import *
from bpy_extras.io_utils import ExportHelper


class ExportNear(bpy.types.Operator, ExportHelper):
    bl_idname = "export_scene.json"
    bl_label = "Export Near Scene"

    filename_ext = ".json"
    filter_glob = StringProperty(default="*.json", options={'HIDDEN'})
    
    use_diffuse_only = BoolProperty(name="Diffuse Only", description="Only export diffuse component of material", default=False)
    use_power_mult = FloatProperty(name="Light Power Scaling", description="Scale the power of lights", default=1.0, min=0)
    use_inline_geom = BoolProperty(name="Inline Geometry", description="Inline the geometry data in json text", default=False)
    use_export_material = BoolProperty(name="Export Material", description="Export the material or use the default material", default=True)
    use_apply_modifiers = BoolProperty(name="Apply Modifiers",
    description="Apply modifiers to the models", default=True)
    
    def execute(self, context):
        filepath = self.filepath
        filepath = bpy.path.ensure_ext(filepath, self.filename_ext)
        
        opts = {}
        opts["diffuse_only"] = self.use_diffuse_only
        opts["light_power_mult"] = self.use_power_mult
        opts["inline_geom"] = self.use_inline_geom
        opts["export_material"] = self.use_export_material
        opts["apply_modifiers"] = self.use_apply_modifiers
        
        import io_scene_near.export_near
        return io_scene_near.export_near.save( self, context, opts, filepath )
    
    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.prop(self, "use_diffuse_only")
        row = layout.row()
        row.prop(self, "use_power_mult")
        row = layout.row()
        row.prop(self, "use_inline_geom")
        row = layout.row()
        row.prop(self, "use_export_material")
        row = layout.row()
        row.prop(self, "use_apply_modifiers")

def menu_func(self, context):
    self.layout.operator(ExportNear.bl_idname, text="Near Scene (.json)")


def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_func)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_func)

# NOTES
# - blender version is hardcoded

if __name__ == "__main__":
    register()
