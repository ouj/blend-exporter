import math
from io_scene_near.jxform import *
from io_scene_near.jmaterial import *
from io_scene_near.jshape import *
import os

def array_to_file(filename, a, c):
    f = open(filename, "wb")
    header = str.encode('B' + a.typecode)
    l = int(len(a)/c)
    dim = array('i', [l, 1, c])
    f.write(header)
    dim.tofile(f)
    a.tofile(f)
    f.flush()
    os.fsync(f.fileno()) #force a file write
    f.close()
    
def list_to_file(filename, list, t, c):
    a = array(t, list)
    array_to_file(filename, a, c)
    
def remove_mesh(mesh):
    try:
        mesh.user_clear()
        can_continue = True
    except:
        can_continue = False
    if can_continue == True:
        try:
            bpy.data.meshes.remove(mesh)
            result = True
        except:
            result = False
    else:
        result = False
    return result
    
def write_triangle_surface(shape_name, surface, inline_geom, geom_folder, geom_path):
    jshape = {}
    jshape["name"] = shape_name
    jshape["type"] = "trianglemeshshape"
    if(geom_path == None or inline_geom):
        jshape['pos'] = surface['position']
        jshape["face"] = surface['faces']
        if(surface['normal'] != None and len(surface['normal']) > 0):
            jshape['normal'] = surface['normal']
        if(surface['texcoord'] != None and len(surface['texcoord']) > 0):
            jshape["uv"] = surface['texcoord']
    else:
        jshape["pos_filename"] = os.path.join(geom_folder, shape_name + "_position")
        list_to_file(os.path.join(geom_path, shape_name + "_position"), surface['position'], 'f', 3)
        jshape["face_filename"] = os.path.join(geom_folder, shape_name + "_face")
        list_to_file(os.path.join(geom_path, shape_name + "_face"), surface['faces'], 'i', 3)
        if(surface['normal'] != None and len(surface['normal']) > 0):
            jshape["norm_filename"] = os.path.join(geom_folder, shape_name + "_normal")
            list_to_file(os.path.join(geom_path, shape_name + "_normal"), surface['normal'], 'f', 3) 
        if(surface['texcoord'] != None and len(surface['texcoord']) > 0):
            jshape["uv_filename"] = os.path.join(geom_folder, shape_name + "_texcoord")
            list_to_file(os.path.join(geom_path, shape_name + "_texcoord"), surface['texcoord'], 'f', 2)
    return jshape


def write_curve_surface(shape_name, surface, inline_geom, geom_folder, geom_path):
    jshape = {}
    jshape["name"] = shape_name
    jshape["type"] = "hairmeshshape"
    if(geom_path == None or inline_geom):
        jshape['pos'] = surface['position']
        jshape['tangent'] = surface['tangent']
        jshape['binormal'] = surface['binormal']
        jshape['strand'] = surface['strand']
        jshape["face"] = surface['face']
    else:
        jshape["pos_filename"] = os.path.join(geom_folder, shape_name + "_position")
        list_to_file(os.path.join(geom_path, shape_name + "_position"), surface['position'], 'f', 3)
        jshape["face_filename"] = os.path.join(geom_folder, shape_name + "_face")
        list_to_file(os.path.join(geom_path, shape_name + "_face"), surface['face'], 'i', 3)
        jshape["tangent_filename"] = os.path.join(geom_folder, shape_name + "_tangent")
        list_to_file(os.path.join(geom_path, shape_name + "_tangent"), surface['tangent'], 'f', 3) 
        jshape['binormal_filename'] = os.path.join(geom_folder, shape_name + "_binormal")
        list_to_file(os.path.join(geom_path, shape_name + "_binormal"), surface['binormal'], 'f', 3)
        jshape['strand_filename'] = os.path.join(geom_folder, shape_name + '_strand')
        array_to_file(os.path.join(geom_path, shape_name + '_strand'), surface['strand'], 'i', 1)
    return jshape


    