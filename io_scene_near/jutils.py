JEPSILON = 1e-6
import math

def jequal(a, b):
    #print(math.fabs(a - b))
    return (math.fabs(a - b) <= JEPSILON)

def jvec_to_array(v):
    return [i for i in v]
