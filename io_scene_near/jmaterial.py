import bpy
import shutil
import os
import string
import math

def create_emission_material(jscene, le, name, opts):
    power_mult = opts["light_power_mult"]
    jmaterial = {}
    jmaterial["name"] = name.replace(' ', '_')
    jmaterial["type"] = "lambertemissionmaterial"
    jmaterial["le"] = [ ( x * power_mult ) for x in le ]
    jscene["materials"].append(jmaterial)
    return

def create_default_material(jscene):
    jmaterial = {}
    jmaterial["name"] = 'default_material'
    jmaterial["type"] = "lambertmaterial"
    jmaterial["rhod"] = [ 0.4, 0.4, 0.4 ]
    jscene["materials"].append(jmaterial)
    return


def texture_name(tex):
    return (tex.name + '_tx').replace(' ', '_')

def proc_material(jscene, material, opts):
    diffuse_only = opts["diffuse_only"]
 
 
    roughness = material.specular_hardness
    diffuse_intensity = material.diffuse_intensity
    specular_intensity = (2 * math.pi) / (roughness + 2) * material.specular_intensity
    kd = [diffuse_intensity * v for v in material.diffuse_color]
    ks = [specular_intensity * v for v in material.specular_color]
    
    kdTexName = None
    ksTexName = None
    roughnessTexName = None
    alphaTexName = None
    
    for mt in material.texture_slots:
        if mt == None or mt.texture == None:
            continue
        if mt.texture.type != 'IMAGE' or mt.texture.image == None:
            continue
        if mt.use_map_color_diffuse == True:
            kdTexName = texture_name(mt.texture)
        if mt.use_map_color_spec == True:
            ksTexName = texture_name(mt.texture)
        if mt.use_map_hardness == True:
            roughnessTexName = texture_name(mt.texture)
        if mt.use_map_alpha == True:
            alphaTexName = texture_name(mt.texture)
#        if mt.use_map_color_reflection == True:
#            krtex = proc_textures(mt);
#        if mt.use_map_color_transmission == True:
#            kttex = proc_textures(mt);
#        if (not mt.use_map_color_diffuse) and (not mt.use_map_color_spec) and (not mt.use_map_hardness) and (not mt.use_map_color_reflection) and (not mt.use_map_color_transmission):
#            print ('warning: unsupported texture mapping')

    # Diffused is shared by all the materials
    jmaterial = {}
    jmaterial["name"] = material.name.replace(' ', '_')
    if(kdTexName == None):
        jmaterial["rhod"] = kd
    else:
        jmaterial["rhod"] = [diffuse_intensity] * 3
        jmaterial["rhodtxt_ref"] = kdTexName
    
    # Select the Model
    if(diffuse_only or ( sum(ks) == 0 and ksTexName == None )): 
        # treat as lambert
        jmaterial["type"] = "lambertmaterial"
    else:
        # treat as microfacet
        jmaterial["type"] = "microfacetmaterial"
        jmaterial["diffuse"] = "lambert"
        jmaterial["specular"] = "blinnphong"
        #if material.specular_shader == 'PHONG':
        #    jmaterial["specular"] = "blinnphong"
        #elif material.specular_shader == 'COOKTORR':
        #    jmaterial["specular"] = "cooktorrance"
        #else:
        #    print("unsupported specular model, use phong");
        #    jmaterial["specular"] = "blinnphong"
        if(ksTexName == None):
            jmaterial["rhos"] = ks
        else:
            jmaterial["rhos"] = [specular_intensity] * 3
            jmaterial["rhostxt_ref"] = ksTexName
        
        jmaterial["n"] = roughness
        if(roughnessTexName != None):
            jmaterial["ntxt_ref"] = roughnessTexName

    if(alphaTexName != None):
        jmaterial['opacity_ref'] = alphaTexName    
    jscene["materials"].append(jmaterial)

def proc_materials(jscene, data, opts):
    for material in data.materials:
        proc_material(jscene, material, opts)
    return

def proc_texture(jscene, tex, opts):
    tex_path = opts["tex_path"]
    tex_folder = opts["tex_folder"]
    
    if tex.type != 'IMAGE' or tex.image == None:
        #print ('warning: only image type texture is supported')
        return None

    absPath = bpy.path.abspath(tex.image.filepath_raw)
    if not os.path.exists(absPath):
        print ('texture: ' + tex.name + '. image file: ' + tex.image.filepath + ' is not exist')
        return None
    
    shutil.copy(absPath, tex_path)
    tex_filename = bpy.path.display_name_from_filepath(absPath)
    (basename, extension) = os.path.splitext(absPath)
    extension = extension.lower()
    if extension == '.exr' or extension == '.tga':
        tex_filename = tex_filename + '.pfm'
    if extension == '.jpg' or extension == '.png' or extension == '.bmp':
        tex_filename = tex_filename + '.ppm'
    jtexture = {}
    jtexture["name"] = texture_name(tex)
    jtexture["type"] = "texture"
    jtexture["image_filename"] = tex_folder + tex_filename
    if tex.extension == 'REPEAT':
        jtexture["wrap"] = "repeat"
    elif tex.extension == 'EXTEND':
        jtexture["wrap"] = "clamp"
    elif tex.extension == 'CLIP':
        jtexture["wrap"] = "black"
    else:
        print("unsupported texture wrapping" + tex.extension + ". use repeat")
        jtexture["wrap"] = "repeat"
    return jtexture

def proc_textures(jscene, data, opts):
    jtexture = {}
    for tex in data.textures:
        jtexture = proc_texture(jscene, tex, opts)
        if(jtexture != None):
            jscene["textures"].append(jtexture)
    return