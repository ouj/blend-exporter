#!/usr/bin/python

import os
import glob
import sys
import subprocess

def convert_images(path):
    _environ = dict(os.environ) #save previous
    os.environ['PATH'] += ':/usr/local/bin' # add local usr bin

    path = os.path.normpath(path)

    ext_8bit = [".[Jj][Pp][Gg]", ".[Pp][Nn][Gg]", ".[Bb][Mm][Pp]", ".[Tt][Ii][Ff]", ".[Jj][Pp][Ee][Gg]"]
    image_8bit = []

    for ext in ext_8bit:
        img_path = os.path.normpath(os.path.join(path, "*" + ext))
        gp = glob.glob(img_path)
        image_8bit += gp

    for fname in image_8bit:
        if os.path.isfile(fname):
            (base, extension) = os.path.splitext(fname)
            nfname = base + ".ppm"
            bmpname = base + ".bmp" #work around for the stupid ppm comment, convert to bmp first
            cmd = ['convert', fname, bmpname]
            subprocess.call(cmd)
            cmd = ['convert', '-gamma', '.454545455', bmpname, nfname] #counter gamma correction
            subprocess.call(cmd)
            cmd = ['rm', bmpname]
            subprocess.call(cmd)
            cmd = ['rm', fname]
            subprocess.call(cmd)

    ext_hdr = [".[Ee][Xx][Rr]", ".[Tt][Gg][Aa]", ".[Hh][Dd][Rr]"]
    image_hdr = []

    for ext in ext_hdr:
        img_path = os.path.normpath(os.path.join(path, "*" + ext))
        gp = glob.glob(img_path)
        image_hdr += gp

    for fname in image_hdr:
        if os.path.isfile(fname):
            (base, extension) = os.path.splitext(fname)
            nfname = base + ".pfm"
            cmd = "hdrconv " + fname + " " + nfname
            os.system(cmd)

    os.environ.clear() #revserse to previous
    os.environ.update(_environ)

def main(argv = None):
    if argv == None or len(argv) <= 1:
        path = ''
    else:
        path = sys.argv[1]


if __name__ == '__main__':
    main(sys.argv)
