from io_scene_near.jxform import *
from io_scene_near.jsurface import create_quad_shape
from io_scene_near.jmaterial import create_emission_material

def proc_point_light(jscene, data, light_name, xform_name, opts):
    power_mult = opts["light_power_mult"]
    jlight = {}
    jlight["name"] = light_name
    jlight["type"] = "pointlight"
    jlight["intensity"] = [(data.energy * v * power_mult) for v in data.color]
    jlight["transform_ref"] = xform_name
    return jlight

def proc_directional_light(jscene, data, light_name, xform_name, opts):
    jlight = {}
    jlight["name"] = light_name
    jlight["type"] = "directionallight"
    jlight["radiance"] = [(data.energy * v) for v in data.color]
    jlight["transform_ref"] = xform_name
    return jlight
    
    
def proc_area_light(jscene, data, light_name, xform_name, opts):
    shape_name = (light_name + '_sh').replace(' ', '_')
    material_name = (light_name + '_m').replace(' ', '_')
    
    le = [(data.energy * v) for v in data.color]
    create_emission_material(jscene, le, material_name, opts)
    
    size_y = size_x = data.size
    if(data.shape == 'RECTANGLE'):
        size_y = data.size_y
    create_quad_shape(jscene, size_x, size_y, shape_name)
    
    jlight = {}
    jlight["name"] = light_name
    jlight["type"] = "arealight"
    jlight["shape_ref"] = shape_name
    jlight["material_ref"] = material_name
    jlight["transform_ref"] = xform_name
    return jlight

def proc_env_light(jscene, data, light_name, xform_name, opts):
    jlight = {}
    jlight["name"] = light_name
    jlight["type"] = "environmentlight"
    jlight["le"] = [(data.energy * v) for v in data.color]
    jlight["transform_ref"] = xform_name
    return jlight

def proc_light(jscene, light, opts):
    xform_name = (light.name + '_t').replace(' ', '_')
    light_name = light.name.replace(' ', '_')
    
    location = light.location
    rotation = light.rotation_euler
    jxform = proc_transform_invert_z(location, rotation, xform_name)
    
    jlight = None
    data = light.data
    if data.type == 'POINT':
        jlight = proc_point_light(jscene, data, light_name, xform_name, opts)
    elif data.type == 'AREA':
        jlight = proc_area_light(jscene, data, light_name, xform_name, opts)
    elif data.type == 'SPOT':
        print('spot light is not supported yet, use a point light')
        jlight = proc_point_light(jscene, data, light_name, xform_name, opts)
    elif data.type == 'SUN':
        jlight = proc_directional_light(jscene, data, light_name, xform_name, opts)
    elif data.type == 'HEMI':
        jlight = proc_env_light(jscene, data, light_name, xform_name, opts)
    
    jscene["transforms"].append(jxform)
    jscene["lights"].append(jlight)
    