import math
import bpy
from io_scene_near.jxform import *

def proc_camera(jscene, scene, opts):
    camera = scene.camera
    context = scene.render
    ratio = float(context.resolution_y) / float(context.resolution_x)
    fov = camera.data.angle / 2.0
    distance = 1.0
    width = distance * math.tan(fov) * 2.0
    height = width * ratio
    
    xform_name = (camera.name + '_t').replace(' ', '_')
    
    jcamera = {}
    jcamera["name"] = camera.name.replace(' ', '_')
    jcamera["type"] = "perspectivecamera"
    jcamera["transform_ref"] = xform_name
    jcamera["width"] = width
    jcamera["height"] = height
    jcamera["distance"] = distance
    jcamera["aperture"] = 0.0
    
    location = camera.location
    rotation = camera.rotation_euler
    jxform = proc_transform_invert_z(location, rotation, xform_name)
    
    jscene["cameras"].append(jcamera)
    jscene["transforms"].append(jxform)
