import math
from io_scene_near.jxform import *
from io_scene_near.jmaterial import *
from array import array

def get_vec(a, e):
    return (a[e*3+0], a[e*3+1], a[e*3+2])

def set_vec(a, e, v):
    a[e*3+0] = v[0]
    a[e*3+1] = v[1]
    a[e*3+2] = v[2]

def sub_vec(a, b):
    return (a[0]-b[0], a[1]-b[1], a[2]-b[2])
    
def add_vec(a, b):
    return (a[0]+b[0], a[1]+b[1], a[2]+b[2])
    
def length_vec(a):
    return math.sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2])

def normalize_vec(a):
    l = length_vec(a)
    if l == 0.0: return 0.0
    else: return (a[0]/l,a[1]/l,a[2]/l)
    
def cross_vec(a, b):
    return (a[1]*b[2]-a[2]*b[1], -a[0]*b[2]+a[2]*b[0], a[0]*b[1]-a[1]*b[0])
    
def dot_vec(a, b):
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2]

def local_frame(z):
	x = (1.0, 0.0, 0.0);
	x = cross_vec(z, x);
	if(dot_vec(x,x) == 0):
		x = normalize_vec(cross_vec(z, (0,1,0)));
	else:
		x = normalize_vec(x);
	y = cross_vec(z, x);
	return (x, y, z)
'''
    if abs(z[0]) > abs(z[1]) :
        invLen = 1.0 / math.sqrt(z[0]*z[0] + z[2]*z[2]);
        x = (-z[2]*invLen, 0.0, z[0]*invLen)
    else:
        invLen = 1.0 / math.sqrt(z[1]*z[1] + z[2]*z[2]);
        x = (0.0, z[2]*invLen, -z[1]*invLen)
    y = cross_vec(z, x);
    return (x, y, z)
'''
    

def create_quad_shape(jscene, size_x, size_y, shape_name):
    jshape = {}
    jshape["name"] = shape_name
    jshape["type"] = "quadshape"
    jshape["width"] = size_x
    jshape["height"] = size_y
    jscene["shapes"].append(jshape)

def triangulate_face(tris):
    v = tris.vertices
    newFaces = None
    if len(v) == 3:
        newFaces = [ int(v[0]), int(v[1]), int(v[2]) ]
    elif len(v) == 4:
        newFaces = [ int(v[0]), int(v[1]), int(v[2]), int(v[0]), int(v[2]), int(v[3]) ]
    else:
        print('Unsupported face type: %d' % len(v))
    return newFaces
    
def proc_shape_tri(shape, scale):
    verts = shape.vertices
    triangles = shape.polygons
    indices = set([f.material_index for f in triangles])
    surfaces = []
    smooth = False
    if (len(verts) < 3) or (len(triangles) < 1):
        print ('empty mesh')
        return (None, None, None, None)

    for m in indices:
        surface = {}
        surface['faces'] = array('i')
        smooth = False
    
        for tris in triangles:
            if tris.material_index == m:
                if (tris.use_smooth):
                    smooth = True
                newFaces = triangulate_face(tris)
                if newFaces != None:
                    surface['faces'].fromlist()
            
        if m < len(shape.materials) and shape.materials[m] != None:
            surface['material'] = shape.materials[m].name.replace(' ', '_')
        else:
            surface['material'] = 'default_material'
        surfaces.append(surface)
        
    positions = array('f')
    normals = array('f')
    texcoords = array('f')
    for v in verts:
        p = [v.co.x * scale.x, v.co.y * scale.y, v.co.z * scale.z]
        positions.fromlist(p)
        if (smooth == True):
            norm = Vector()
            norm.x = v.normal.x / scale.x
            norm.y = v.normal.y / scale.y
            norm.z = v.normal.z / scale.z
            norm.normalize()
            normals.fromlist([x for x in norm])
    
    if (shape.uv_textures.active != None):
        uvs = [0] * len(verts) * 2
        uv_data = shape.uv_layers.active.data
        for f in triangles:
            for i in f.loop_indices:
                suv = uv_data[i].uv
                vi = f.vertices[i - f.loop_start]
                uvs[vi * 2 + 0] = suv[0]
                uvs[vi * 2 + 1] = suv[1]
        texcoords += uvs

    return (surfaces, positions, normals, texcoords)

def proc_shape_tri_split(shape, scale):
    verts = shape.vertices
    triangles = shape.polygons
    indices = set([f.material_index for f in triangles])
    surfaces = []
    smooth = False
    if (len(verts) < 3) or (len(triangles) < 1):
        print ('empty mesh')
        return (None, None, None, None)

    positions = []
    normals = []
    texcoords = []
    for v in verts:
        p = [v.co.x * scale.x, v.co.y * scale.y, v.co.z * scale.z]
        positions += p
        norm = Vector()
        norm.x = v.normal.x / scale.x
        norm.y = v.normal.y / scale.y
        norm.z = v.normal.z / scale.z
        norm.normalize()
        normals += [x for x in norm]
    
    if (shape.uv_textures.active != None):
        uvs = [0] * len(verts) * 2
        uv_data = shape.uv_layers.active.data
        for f in triangles:
            for i in f.loop_indices:
                suv = uv_data[i].uv
                vi = f.vertices[i - f.loop_start]
                uvs[vi * 2 + 0] = suv[0]
                uvs[vi * 2 + 1] = suv[1]
        texcoords += uvs
 
    for m in indices:
        surface = {}
        surface['faces'] = []
        surface['position'] = []
        surface['normal'] = []
        surface['texcoord'] = []
        smooth = False
    
        flag = [-1] * len(verts)
        k = 0
        for tris in triangles:
            if tris.material_index == m:
                if (tris.use_smooth):
                    smooth = True
                face = triangulate_face(tris)
                if face == None: continue
                for f in face:
                    if flag[f] == -1:
                        flag[f] = k
                        k = k + 1
                        surface['position'] += [positions[f*3+0],positions[f*3+1],positions[f*3+2]]
                        surface['normal'] += [normals[f*3+0],normals[f*3+1],normals[f*3+2]]
                        if len(texcoords) > 0:
                            surface['texcoord'] += [texcoords[f*2+0],texcoords[f*2+1]]
                    surface['faces'].append(flag[f])
        if smooth == False: surface['normal'] = []
            
        if m < len(shape.materials) and shape.materials[m] != None:
            surface['material'] = shape.materials[m].name.replace(' ', '_')
        else:
            surface['material'] = 'default_material'
        surfaces.append(surface)

    return surfaces
    
'''
def proc_flatten_shape_tri(shape, matrix):
    verts = shape.vertices
    triangles = shape.faces
    indices = set([f.material_index for f in triangles])
    
    inv_transpose_matrix = matrix.inverted().transposed()
    
    if (len(verts) < 3) or (len(triangles) < 1):
        print ('empty mesh')
        return (None, None, None, None)
        
    surfaces = []
    for m in indices:
        surface = {}
        positions = array('f')
        normals = array('f')
        texcoords = array('f')
        smooth = False
    
        surface['faces'] = array('i')
        for tris in triangles:
            if tris.material_index == m:
                if (tris.use_smooth):
                    smooth = True
                surface['faces'].fromlist(triangulate_face(tris))

        for v in verts:
            nco = matrix * v.co
            p = [nco.x, nco.y, nco.z]
            positions.fromlist(p)
            if (smooth == True):
                nnorm = inv_transpose_matrix * v.normal
                nnorm.normalize()
                normals.fromlist([x for x in nnorm])
        
        if (shape.uv_textures.active != None):
            uv_data = shape.uv_textures.active.data
            uvs = [0] * len(verts) * 2
            for triIdx in range(len(triangles)):
                sface = triangles[triIdx]
                suvs = uv_data[triIdx].uv
                for idx in range(len(suvs)):
                    uvs[sface.vertices[idx] * 2 + 0] = suvs[idx][0]
                    uvs[sface.vertices[idx] * 2 + 1] = suvs[idx][1]
            texcoords.fromlist(uvs)
            
        if m < len(shape.materials) and shape.materials[m] != None:
            surface['material'] = shape.materials[m].name.replace(' ', '_')
        else:
            surface['material'] = 'default_material'

        surfaces.append(surface)
    return (surfaces, positions, normals, texcoords)
'''

def proc_spline(spline, radius, resolution, spline_id):
    nsp = len(spline.points)
    sp = spline.points
    if nsp < 2: return (None, None, None, None)
    
    # compute tangents
    tang = [0.0] * (nsp * 3)
    set_vec(tang, 0, normalize_vec(sub_vec(sp[1].co, sp[0].co)))
    set_vec(tang, nsp - 1, normalize_vec(sub_vec(sp[nsp-1].co, sp[nsp-2].co)))
    
    for i in range(1, nsp-1):
        t = add_vec(sub_vec(sp[i].co, sp[i-1].co), 
                    sub_vec(sp[i+1].co, sp[i].co))
        set_vec(tang, i, normalize_vec(t))
        
    pos = []
    face = []
    tangent = []
    binormal = []
    strand = []
    
    circle = []
    ncp = 2 * (resolution + 2)
    
    for i in range(ncp):
        angle = i/ncp * 2 * math.pi
        circle.append((math.cos(angle) * radius, math.sin(angle) * radius, 0))
    #print (circle)
    
    for i in range(nsp):
        t = get_vec(tang, i)
        p = sp[i].co;
        (x, y, z) = local_frame(t)
        for c in circle:
            pt = (dot_vec(x, c), dot_vec(y, c), dot_vec(z, c))
            pt = add_vec(pt, p)
            pos += (list(pt))
            tangent += (list(z))
            binormal += (list(x))  
    
    for i in range(nsp-1):
        for c in range(ncp-1):
            face.append(i*ncp+c+0)
            face.append((i+1)*ncp+c+0)
            face.append(i*ncp+c+1)
            strand.append(spline_id)
            
            face.append(i*ncp+c+1)
            face.append((i+1)*ncp+c+0)
            face.append((i+1)*ncp+c+1)
            strand.append(spline_id)
            
        face.append((i+1)*ncp-1)
        face.append((i+2)*ncp-1)
        face.append(i*ncp)
        strand.append(spline_id)
        
        face.append(i*ncp)
        face.append((i+2)*ncp-1)
        face.append((i+1)*ncp)
        strand.append(spline_id)
    #print (face)
    
    return (pos, face, tangent, binormal, strand)
    

def proc_shape_curve(shape, scale):
    if len(shape.splines) == 0: return None

    positions = []
    tangents = []
    binormals = []
    strand = []
    faces = []
    
    resolution = shape.bevel_resolution
    radius = shape.bevel_depth
    
    poscount = 0
    spline_id = 0
    for spline in shape.splines:
        (pos, face, tangent, binormal, strand) = proc_spline(spline, radius, resolution, spline_id)
        if pos == None or face == None: continue
        positions += pos
        tangents += tangent
        binormals += binormal
        face = [f+poscount for f in face]
        poscount += int(len(pos) / 3)
        strand += strand
        spline_id = spline_id + 1
        faces += face
        
    if len(shape.materials) >= 1:
        material = shape.materials[0].name.replace(' ', '_')
    else:
        material = 'default_material'
    
    return {'face':faces, 'position':positions, 'tangent':tangents, 'binormal':binormals, 'strand':strand, 'material':material}


def array_to_file(filename, a, c):
    f = open(filename, "wb")
    header = str.encode('B' + a.typecode)
    l = int(len(a)/c)
    dim = array('i', [l, 1, c])
    f.write(header)
    dim.tofile(f)
    a.tofile(f)
    f.flush()
    os.fsync(f.fileno()) #force a file write
    f.close()
    
def list_to_file(filename, list, t, c):
    a = array(t, list)
    array_to_file(filename, a, c)
    
def remove_mesh(mesh):
    try:
        mesh.user_clear()
        can_continue = True
    except:
        can_continue = False
    if can_continue == True:
        try:
            bpy.data.meshes.remove(mesh)
            result = True
        except:
            result = False
    else:
        result = False
    return result
    
def write_triangle_surface(shape_name, surface, inline_geom, geom_folder, geom_path):
    jshape = {}
    jshape["name"] = shape_name
    jshape["type"] = "trianglemeshshape"
    if(geom_path == None or inline_geom):
        jshape['pos'] = surface['position']
        jshape["face"] = surface['faces']
        if(surface['normal'] != None and len(surface['normal']) > 0):
            jshape['normal'] = surface['normal']
        if(surface['texcoord'] != None and len(surface['texcoord']) > 0):
            jshape["uv"] = surface['texcoord']
    else:
        jshape["pos_filename"] = os.path.join(geom_folder, shape_name + "_position")
        list_to_file(os.path.join(geom_path, shape_name + "_position"), surface['position'], 'f', 3)
        jshape["face_filename"] = os.path.join(geom_folder, shape_name + "_face")
        list_to_file(os.path.join(geom_path, shape_name + "_face"), surface['faces'], 'i', 3)
        if(surface['normal'] != None and len(surface['normal']) > 0):
            jshape["norm_filename"] = os.path.join(geom_folder, shape_name + "_normal")
            list_to_file(os.path.join(geom_path, shape_name + "_normal"), surface['normal'], 'f', 3) 
        if(surface['texcoord'] != None and len(surface['texcoord']) > 0):
            jshape["uv_filename"] = os.path.join(geom_folder, shape_name + "_texcoord")
            list_to_file(os.path.join(geom_path, shape_name + "_texcoord"), surface['texcoord'], 'f', 2)
    return jshape


def write_curve_surface(shape_name, surface, inline_geom, geom_folder, geom_path):
    jshape = {}
    jshape["name"] = shape_name
    jshape["type"] = "hairmeshshape"
    if(geom_path == None or inline_geom):
        jshape['pos'] = surface['position']
        jshape['tangent'] = surface['tangent']
        jshape['binormal'] = surface['binormal']
        jshape['strand'] = surface['strand']
        jshape["face"] = surface['face']
    else:
        jshape["pos_filename"] = os.path.join(geom_folder, shape_name + "_position")
        list_to_file(os.path.join(geom_path, shape_name + "_position"), surface['position'], 'f', 3)
        jshape["face_filename"] = os.path.join(geom_folder, shape_name + "_face")
        list_to_file(os.path.join(geom_path, shape_name + "_face"), surface['face'], 'i', 3)
        jshape["tangent_filename"] = os.path.join(geom_folder, shape_name + "_tangent")
        list_to_file(os.path.join(geom_path, shape_name + "_tangent"), surface['tangent'], 'f', 3) 
        jshape['binormal_filename'] = os.path.join(geom_folder, shape_name + "_binormal")
        list_to_file(os.path.join(geom_path, shape_name + "_binormal"), surface['binormal'], 'f', 3)
        jshape['strand_filename'] = os.path.join(geom_folder, shape_name + '_strand')
        array_to_file(os.path.join(geom_path, shape_name + '_strand'), surface['strand'], 'i', 1)
    return jshape
