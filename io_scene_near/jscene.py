import os
from io_scene_near.jcamera import proc_camera
from io_scene_near.jlight import proc_light
from io_scene_near.jsurface import proc_surface
from io_scene_near.jsurface import proc_curve
from io_scene_near.jpsystem import proc_psystem
from io_scene_near.jmaterial import proc_materials
from io_scene_near.jmaterial import create_default_material
from io_scene_near.jxform import create_identity_transform
from io_scene_near.jmaterial import proc_textures


def proc_lights(jscene, scene, opts):
    for object in scene.objects:
        if object.type == 'LAMP' and \
           object.is_visible(scene) == True and \
           object.hide_render == False:         
            proc_light(jscene, object, opts)
    
def proc_surface_objects(jscene, scene, data, opts):
    for object in scene.objects:
        if object.is_visible(scene) == False: continue
        if object.hide_render == True: continue
        if object.type == 'MESH':
            proc_surface(jscene, scene, object, opts)
        if object.type == 'CURVE':
            proc_curve(jscene, scene, object, opts)
        if object.type == 'EMPTY' and object.dupli_group != None:
            print ("instancing is not supported, use the binary version")

def proc_psystems(jscene, scene, data, opts):
    for object in scene.objects:
        if object.is_visible(scene) == False: continue
        if object.hide_render == True: continue
        if len(object.particle_systems) == 0: continue
        proc_psystem(jscene, scene, object, opts)
    
def jscene_init(scene, data):
    jscene = {}
    jscene["name"] = scene.name
    jscene["transforms"] = []
    jscene["lights"] = []
    jscene["cameras"] = []
    jscene["textures"] = []
    jscene["materials"] = []
    jscene["shapes"] = []
    jscene["surfaces"] = []
    jscene["settings"] = { 'rayepsilon': 0.001 }
    return jscene
    
def insuredir(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)
    if not os.path.isdir(dirpath):
        print("error: " + dirpath + " is not a directory\n")
        return False
    return True

def json_scene(scene, data, dirname, basename, opts):
    (filename, ext) = os.path.splitext(basename)
    inc_path = os.path.join(dirname,filename)
    
    opts["inc_path"] = inc_path
    opts["geom_path"] = os.path.join(inc_path,"geometry/")
    opts["geom_folder"] = filename + "/geometry/";
    opts["tex_path"] = os.path.join(inc_path,"texture/")
    opts["tex_folder"] = filename + "/texture/";
    
    geom_path = opts["geom_path"]
    if not insuredir(geom_path): return
    print("geometry path: " + geom_path);
    
    tex_path = opts["tex_path"]
    if not insuredir(tex_path): return
    print("texture path:  " + tex_path);
    
    jscene = jscene_init(scene, data)
    create_default_material(jscene)
    create_identity_transform(jscene)
    proc_camera(jscene, scene, opts)
    if opts["export_material"]:
        proc_materials(jscene, data, opts)
        proc_textures(jscene, data, opts)
    proc_lights(jscene, scene, opts)
    proc_surface_objects(jscene, scene, data, opts)
    proc_psystems(jscene, scene, data, opts)
    return jscene
