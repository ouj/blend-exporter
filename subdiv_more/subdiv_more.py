bl_info = {
    "name": "Subdiv More",
    "author": "Jiawei Ou",
    "blender": (2, 5, 6),
    "api": 35357,
    "location": "Object",
    "description": "Increase the subdiv level of all subdiv modifier",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Object"}

if "bpy" in locals():
    import imp
    if "subdiv_more" in locals():
        imp.reload(subdiv_more)

import os
import bpy
from bpy.props import *

def subdiv_more(opts):
    level = opts['level']
    do_view = opts['do_view']
    for object in bpy.data.objects:
        if object.type == 'MESH':
            for md in object.modifiers:
                if md.type == 'SUBSURF':
                    if do_view:
                        md.levels = max(0, md.levels + level)
                    md.render_levels = max(0, md.render_levels + level)
    return

class SubdivMore(bpy.types.Operator):
    bl_idname = "object.subdiv_more"
    bl_label = "Subdiv More"
    
    use_do_view_subdiv = BoolProperty(name="Do the view subdiv", description="also increase the view subdiv", default=False)
    use_level = IntProperty(name="Level delta", description="the change to the level", default=1)
    
    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)

    def execute(self, context):
        opts = {}
        opts['do_view'] = self.use_do_view_subdiv
        opts["level"] = self.use_level

        subdiv_more(opts)
        return {'FINISHED'}

def register():
    bpy.utils.register_class(SubdivMore)

def unregister():
    return
