import math
import bpy
from io_scene_near.jxform import *

def proc_camera(jscene, scene, opts):
    camera = scene.camera
    context = scene.render
    ratio = float(context.resolution_y) / float(context.resolution_x)
    fov = camera.data.angle / 2.0
    distance = 1.0
    width = distance * math.tan(fov) * 2.0
    height = width * ratio
    
    lens_name = (camera.name + '_l').replace(' ', '_')
    xform_name = (camera.name + '_t').replace(' ', '_')
    
    jcamera = {}
    jcamera["name"] = camera.name.replace(' ', '_')
    jcamera["type"] = "camera"
    jcamera["transform_ref"] = xform_name
    jcamera["lens_ref"] = lens_name
    
    jlens = {}
    jlens["name"] = lens_name
    jlens["type"] = "perspectivelens"
    jlens["width"] = width
    jlens["height"] = height
    jlens["distance"] = distance
    jlens["aperture"] = 0.0
    
    location = camera.location
    rotation = camera.rotation_euler
    jxform = proc_transform_invert_z(location, rotation, xform_name)
    
    jscene["cameras"].append(jcamera)
    jscene["lenses"].append(jlens)
    jscene["transforms"].append(jxform)
