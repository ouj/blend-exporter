import os
import bpy
import mathutils
import json
import time

from io_scene_near.jscene import json_scene 

def save(operator, context, opts, filepath):
    path = os.path.normpath(operator.filepath)
    (pathname, ext) = os.path.splitext(path)
    if(ext != '.json'):
        path = path + ".json"
    basename = os.path.basename(path)
    dirname = os.path.dirname(path)
    
    scene = bpy.context.scene
    data = bpy.data

    start = time.clock()
    jscene = json_scene(scene, data, dirname, basename, opts)
    scene_string = json.dumps(jscene, indent=4, sort_keys=True)
    
    f = open(path, "w", newline='\n')
    f.write(scene_string)
    f.close()
    
    print ("camera of scene: " + basename + ' exported, takes ' + str(time.clock()-start) + ' time unit')
    return {'FINISHED'}

