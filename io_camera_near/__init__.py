# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

bl_info = {
    "name": "Near Camera",
    "author": "Jiawei Ou",
    "blender": (2, 5, 6),
    "api": 35357,
    "location": "File > Export",
    "description": "Export Near Camera",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export"}

if "bpy" in locals():
    import imp
    if "export_near" in locals():
        imp.reload(export_near)


import os
import bpy
from bpy.props import *
from bpy_extras.io_utils import ExportHelper


class ExportNear(bpy.types.Operator, ExportHelper):
    bl_idname = "export_camera.json"
    bl_label = "Export Near Camera"

    filename_ext = ".json"
    filter_glob = StringProperty(default="*.json", options={'HIDDEN'})
        
    def execute(self, context):
        filepath = self.filepath
        filepath = bpy.path.ensure_ext(filepath, self.filename_ext)
        
        opts = {}
        
        import io_scene_near.export_near
        return io_scene_near.export_near.save( self, context, opts, filepath )
    
    def draw(self, context):
        layout = self.layout

def menu_func(self, context):
    self.layout.operator(ExportNear.bl_idname, text="Near Camera (.json)")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_func)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_func)

# NOTES
# - blender version is hardcoded

if __name__ == "__main__":
    register()
