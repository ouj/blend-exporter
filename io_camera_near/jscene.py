import os
from io_scene_near.jcamera import proc_camera

    
def jscene_init(scene, data):
    jscene = {}
    jscene["name"] = scene.name
    jscene["transforms"] = []
    jscene["cameras"] = []
    jscene["lenses"] = []
    return jscene
    
def insuredir(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)
    if not os.path.isdir(dirpath):
        print("error: " + dirpath + " is not a directory\n")
        return False
    return True

def json_scene(scene, data, dirname, basename, opts):
    (filename, ext) = os.path.splitext(basename)
    inc_path = os.path.join(dirname,filename)
            
    jscene = jscene_init(scene, data)
    proc_camera(jscene, scene, opts)

    return jscene
